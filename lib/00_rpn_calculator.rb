class RPNCalculator
  attr_reader :value

  def initialize
    @stack = []
    @value = 0
  end

  def push(num)
    @stack.push(num)
  end

  def pop
    @stack.pop
  end

  def value
    @stack.last
  end

  def plus
    raise "calculator is empty" if @stack.size < 2
    nums = [pop, pop]
    @stack.push(nums[1] + nums[0])
  end

  def minus
    raise "calculator is empty" if @stack.size < 2
    nums = [pop, pop]
    @stack.push(nums[1] - nums[0])
  end

  def times
    raise "calculator is empty" if @stack.size < 2
    nums = [pop, pop]
    @stack.push(nums[1] * nums[0])
  end

  def divide
    raise "calculator is empty" if @stack.size < 2
    nums = [pop, pop]
    @stack.push(nums[1].to_f / nums[0])
  end

  def tokens(str)
    str.split.map { |char| %w(+ - * /).include?(char) ? char.to_sym : char.to_i }
  end

  def evaluate(str)
    tokens(str).each do |tok|
      if tok.is_a?(Integer)
        push(tok)
      else
        nums = [pop, pop]
        @stack.push(nums[1].to_f.send(tok, nums[0]))
      end
    end
    value
  end
end
